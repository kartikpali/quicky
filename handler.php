<?php
/**
 * Created by PhpStorm.
 * User: kartikey.pali
 * Date: 4/3/15
 * Time: 5:11 PM
 */
require '../app/Mage.php';
Mage::app();

Class QuickyHandler {


	public function run() {
		$key = (isset($_GET['key']) ? $_GET['key'] : '');
		if (!empty($key)) {
			return $this->_runByKey($key);
		} else {
			return array('success' => false, 'message' => 'Invalid Data');
		}
	}


	private function _runByKey($key) {
		switch ($key) {
			case 'thtafon':
				$this->_thtaf('default', 0, 'dev/debug/template_hints', 1);
				break;

			case 'thtafoff':
				$this->_thtaf('default', 0, 'dev/debug/template_hints', 0);
				break;

			case 'thtfon':
				$this->_thtaf('stores', 1, 'dev/debug/template_hints', 1);
				break;

			case 'thtfoff':
				$this->_thtaf('stores', 1, 'dev/debug/template_hints', 0);
				break;

			case 'thbafon':
				$this->_thtaf('default', 0, 'dev/debug/template_hints_blocks', 1);
				break;

			case 'thbafoff':
				$this->_thtaf('default', 0, 'dev/debug/template_hints_blocks', 0);
				break;

			case 'thbfon':
				$this->_thtaf('stores', 1, 'dev/debug/template_hints_blocks', 1);
				break;

			case 'thbfoff':
				$this->_thtaf('stores', 1, 'dev/debug/template_hints_blocks', 0);
				break;

			case 'fc':
				$this->_clearCache();
				break;

			case 'apcfc':
				$this->_clearApcCache();
				break;

            case 'getmodel':
            case 'gethelper':
            case 'getblock':
            case 'getcollectionbymodel':
            case 'getcollectionbyresourcemodel':
                $resp = $this->_getClassName($key);
                return $resp;
                break;

            case 'loadproduct':
                $resp = $this->_loadProduct();
                return $resp;
                break;
			
			default:
                return array('success' => false, 'message' => "Invalid Data");
				break;
		}

		return array('success' => true, 'message' => 'Valid');
	}

	private function _thtaf($scope, $scopeId, $path, $val) {
        if ($scope == 'stores') {
            $scopeId = Mage::app()->getStore()->getStoreId();
        }
	    $resource = Mage::getSingleton('core/resource');
	     
	    $writeConnection = $resource->getConnection('core_write');

	    $query = "INSERT INTO core_config_data (scope, scope_id, path, value) VALUES ('{$scope}', {$scopeId}, '{$path}', {$val}) ON DUPLICATE KEY UPDATE value=VALUES(value)";
	    
	    $writeConnection->query($query);

	}

	private function _clearCache() {
        Mage::getModel('enterprise_pagecache/observer')->cleanCache();
		Mage::getModel('enterprise_pagecache/observer')->cleanExpiredCache();
		Mage::app()->getCacheInstance()->flush();
    }

    private function _clearApcCache() {
    	if (!function_exists('apc_clear_cache')) {
	        apc_clear_cache();
			apc_clear_cache('user');
			apc_clear_cache('opcode');
		}
    }

    private function _getClassName($type) {
        if (isset($_POST['name']) && !empty($_POST['name'])) {
            $name = $_POST['name'];
            switch ($type) {
                case 'getmodel':
                    $class = get_class(Mage::getModel($name));
                    break;

                case 'gethelper':
                    $class = get_class(Mage::helper($name));
                    break;

                case 'getcollectionbymodel':
                    $preCheck = get_class(Mage::getModel($name));
                    if (!empty($preCheck)) {
                        $class = get_class(Mage::getModel($name)->getCollection());
                    } else {
                        $class = false;
                    }
                    break;

                case 'getcollectionbyresourcemodel':
                    if (substr($name,-11) == '_collection') {
                        $class = get_class(Mage::getResourceModel($name));
                    } else {
                        $preCheck = get_class(Mage::getResourceModel($name));
                        if (!empty($preCheck)) {
                            $class = get_class(Mage::getResourceModel($name.'_collection'));
                        } else {
                            $class = false;
                        }
                    }
                    break;

                case 'getblock':
                    $class = get_class(Mage::app()->getLayout()->createBlock($name));
                    break;

                case 'loadproduct':
                    $resp = $this->_getClassName($key);
                    return $resp;
                    break;

                default:
                    return array('success' => false, 'message' => "Invalid Data");
                    break;
            }

            if (empty($class)) {
                $class = 'Not Found';
            }
            $resp = array('success' => true, 'message' => "Class Name :- <strong>{$class}</strong>");
        } else {
            $resp = array('success' => false, 'message' => "Please Enter The Required Input Field");
        }

        return $resp;
    }


    private function _loadProduct() {
        if (isset($_POST['pid']) && !empty($_POST['pid'])) {
            $pid = $_POST['pid'];
            $product = Mage::getModel('catalog/product')->load($pid);
            if ($product->getId()) {
                $product = $product->getData();
                $html = '';
                $html .= "<table class='table table-striped table-bordered'>";
                foreach ($product as $prod => $value) {
                    if (!is_array($value)) {
                        $html .= "<tr><td>{$prod}</td><td>{$value}</td></tr>";
                    }
                }
                $html .= "</table>";
                $resp = array('success' => true, 'message' => $html);
            } else {
                $html = 'Product Not Found';
                $resp = array('success' => false, 'message' => $html);
            }

        } else {
            $resp = array('success' => false, 'message' => "Please Enter The Required Input Field");
        }

        return $resp;
    }

}
$QuickyHandler = new QuickyHandler();
$quickyResp = $QuickyHandler->run();
echo json_encode($quickyResp);
