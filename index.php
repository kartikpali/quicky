<?php
/**
 * Created by PhpStorm.
 * User: kartikey.pali
 * Date: 4/3/15
 * Time: 4:21 PM
 */
?>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="./assets/bootstrap/css/bootstrap.min.css">
<!--	<link rel="stylesheet" type="text/css" href="./assets/bootstrap/css/bootstrap-theme.min.css">-->
    <link rel="stylesheet" href="./assets/circles.css" type="text/css">

	<!-- Custom CSS -->
    <style>
    body {
        padding-top: 70px;
        /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
    }

    /* Sticky footer styles
-------------------------------------------------- */
    html {
        position: relative;
        min-height: 100%;
    }
    body {
        /* Margin bottom by footer height */
        margin-bottom: 60px;
    }
    .footer {
        position: absolute;
        bottom: 0;
        width: 100%;
        /* Set the fixed height of the footer here */
        height: 60px;
        background-color: #f5f5f5;
    }

    .footer .container .text-muted {
        margin: 20px 0;
    }

    #quickyloader {
        position: fixed;
        width: 100%;
        height:100%;
        display: flex;
        align-items: center;
        top: 0;
    }

    .quickyloaderplus {
        display: flex;
        margin: 0 auto;
    }

    .well:hover{
        -webkit-box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.21);
        -moz-box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.21);
        box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.21);
    }


.btn-primary {
  background-color: #008383;
  border-color: #008383;
}
.btn-primary:hover,
.btn-primary:focus,
.btn-primary:active,
.btn-primary.active {
  background-color: #009c9c;
  border-color: #009c9c;
}
.btn-primary.disabled, 
.btn-primary[disabled],
fieldset[disabled] .btn-primary,
.btn-primary.disabled:hover,
.btn-primary.disabled:focus,
.btn-primary.disabled:active,
.btn-primary.disabled.active,
.btn-primary[disabled]:hover,
.btn-primary[disabled]:focus,
.btn-primary[disabled]:active,
.btn-primary[disabled].active,
fieldset[disabled] .btn-primary:hover,
fieldset[disabled] .btn-primary:focus,
fieldset[disabled] .btn-primary:active,
fieldset[disabled] .btn-primary.active {
  background-color: #008383;
  border-color: #008383;
}

.result-output {
  padding: 10px;
  border: 1px solid #008383;
  border-left-width: 5px;
  border-radius: 3px;
  border-left-color: #008383;
  color:#008383;
}
.result-output:empty { 
    border: none;
    padding: 0px;
}

.alt-a {
    width: 49%;
}

.alt-a.alt {
    width: 100%;
}
    </style>
</head>
<body>

	<!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-lg-12 text-center">
                <h1>A Quick Helper For Magento</h1>
                <p class="lead">For cache, type hinting templates, type hinting blocks.....</p>
            </div>
        </div>
        <!-- /.row -->
        <br><br>
        <div class="row">
			<div class="col-lg-6 text-center">
				<div class="well center-block">
					<h3 class="text-left">Type Hinting Templates <small>Admin & Frontend</small></h3>
			      	<button type="button" class="btn btn-primary alt-a" data-loading-text="Turning ON, Please Wait..." data-key="thtafon">Turn ON</button>
			      	<button type="button" class="btn btn-default alt-a" data-loading-text="Turning OFF, Please Wait..." data-key="thtafoff">Turn OFF</button>
			    </div>

			    <div class="well center-block">
					<h3 class="text-left">Type Hinting Blocks <small>Admin & Frontend</small></h3>
			      	<button type="button" class="btn btn-primary alt-a" data-loading-text="Turning ON, Please Wait..." data-key="thbafon">Turn ON</button>
			      	<button type="button" class="btn btn-default alt-a" data-loading-text="Turning OFF, Please Wait..." data-key="thbafoff">Turn OFF</button>
			    </div>

			    <div class="well center-block">
					<h3 class="text-left">Flush Cache <small>Cache Storage</small></h3>
			      	<button type="button" class="btn btn-primary alt-a alt" data-loading-text="Flushing, Please Wait..." data-key="fc">Flush</button>
			    </div>
			</div>

			<div class="col-lg-6 text-center">
			    <div class="well center-block">
					<h3 class="text-left">Type Hinting Templates <small>Only Frontend</small></h3>
			      	<button type="button" class="btn btn-primary alt-a" data-loading-text="Turning ON, Please Wait..." data-key="thtfon">Turn ON</button>
			      	<button type="button" class="btn btn-default alt-a" data-loading-text="Turning OFF, Please Wait..." data-key="thtfoff">Turn OFF</button>
			    </div>

			    <div class="well center-block">
					<h3 class="text-left">Type Hinting Blocks <small>Only Frontend</small></h3>
			      	<button type="button" class="btn btn-primary alt-a" data-loading-text="Turning ON, Please Wait..." data-key="thbfon">Turn ON</button>
			      	<button type="button" class="btn btn-default alt-a" data-loading-text="Turning OFF, Please Wait..." data-key="thbfoff">Turn OFF</button>
			    </div>


			    <div class="well center-block">
					<h3 class="text-left">APC Flush Cache <small>Cache Storage</small></h3>
			      	<button type="button" class="btn btn-primary alt-a alt" data-loading-text="Flushing, Please Wait..." data-key="apcfc">Flush</button>
			    </div>
			</div>

            <div class="col-lg-12">
                <div class="well">
                    <h3 class="text-left">Get Model Class <small>Get Model Class Name</small></h3>
                    <div class="result-wrapper">
                        <div class="row">
                            <div class="col-sm-10">
                                <input type="text" class="form-control result-input" placeholder="Enter text CLASS-KEY from Mage::getModel('CLASS-KEY');" style="width:100%;">
                                <span class="help-block">eg. catalog/product</span>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary alt-b" data-key="getmodel">Show Class Name</button>
                            </div>
                        </div>
                        <h4 class="result-output"></h4>
                        
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="well">
                    <h3 class="text-left">Get Helper Class <small>Get Helper Class Name</small></h3>
                    <div class="result-wrapper">
                        <div class="row">
                            <div class="col-sm-10">
                                <input type="text" class="form-control result-input" placeholder="Enter text CLASS-KEY from Mage::helper('CLASS-KEY');" style="width:100%;">
                                <span class="help-block">eg. catalog  OR  catalog/data</span>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary alt-b" data-key="gethelper">Show Class Name</button>
                            </div>
                        </div>

                        <h4 class="result-output"></h4>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="well">
                    <h3 class="text-left">Get Block Class <small>Get Block Class Name</small></h3>
                    <div class="result-wrapper">
                        <div class="row">
                            <div class="col-sm-10">
                                <input type="text" class="form-control result-input" placeholder="Enter text CLASS-KEY from <block type='CLASS-KEY' name = 'abc'>" style="width:100%;">
                                <span class="help-block">eg. catalog/navigation  OR  catalog/category_view</span>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary alt-b" data-key="getblock">Show Class Name</button>
                            </div>
                        </div>

                        <h4 class="result-output"></h4>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="well">
                    <h3 class="text-left">Get Collection Class <small>Get Resource Collection Class Name from Mage::getModel()</small></h3>
                    <div class="result-wrapper">
                        <div class="row">
                            <div class="col-sm-10">
                                <input type="text" class="form-control result-input" placeholder="Enter text CLASS-KEY from Mage::getModel('CLASS-KEY');" style="width:100%;">
                                <span class="help-block">eg. catalog/product</span>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary alt-b" data-key="getcollectionbymodel">Show Class Name</button>
                            </div>
                        </div>
                        <h4 class="result-output"></h4>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="well">
                    <h3 class="text-left">Get Collection Class <small>Get Resource Collection Class Name from Mage::getResourceModel()</small></h3>
                    <div class="result-wrapper">
                        <div class="row">
                            <div class="col-sm-10">
                                <input type="text" class="form-control result-input" placeholder="Enter text CLASS-KEY from Mage::getResourceModel('CLASS-KEY');" style="width:100%;">
                                <span class="help-block">eg. catalog/product  OR  catalog/product_collection</span>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary alt-b" data-key="getcollectionbyresourcemodel">Show Class Name</button>
                            </div>
                        </div>
                        <h4 class="result-output"></h4>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="well">
                    <h3 class="text-left">Load Product <small>Load Product Data By Id</small></h3>
                    <div class="result-wrapper">
                        <div class="row">
                            <div class="col-sm-10">
                                <input type="text" class="form-control result-input" placeholder="Enter Product Id" style="width:100%;">
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary alt-c" data-key="loadproduct">Load</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-10"  id="loadprod">
                    </div>
                </div>
            </div>


		</div>

    </div>


    <footer class="footer">
        <div class="container">
            <p class="text-muted text-center">QUICKY By Kartik. . .</p>
        </div>
    </footer>

    <div id="quickyloader" style="display:none;">
        <div class="quickyloaderplus">
            <div class="circles">
                Loading...
            </div>
        </div>
    </div>

	


	<script charset="utf-8" type="text/javascript" src="./assets/jquery-1.11.2.min.js"></script>
	<script charset="utf-8" type="text/javascript" src="./assets/bootstrap/js/bootstrap.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			$('button.alt-a').click(function(){
                $('#quickyloader').show();
				$('button, input').attr('disabled', true);
				var $btn = $(this).button('loading');

				$.ajax({
					url:'./handler.php?key='+$(this).data('key'),
					method: 'post',
                    dataType : 'json',
					success: function(resp) {
                        if (resp.success == false) {
                            alert(resp.message);
                        }
					    $btn.button('reset');
					    $('button, input').attr('disabled', false);
                        $('#quickyloader').hide();
					}
				});
			});

            $('button.alt-b').click(function(){
                var $btn = $(this);
                var name = $btn.parents('div.result-wrapper').find('input.result-input').first().val();
                if  (name=='') {
                    alert('Please Enter The Required Input Field');
                } else {
                    $('#quickyloader').show();
                    $('button, input').attr('disabled', true);
                    $btn.button('loading');
                    $.ajax({
                        url:'./handler.php?key='+$(this).data('key'),
                        data:'name='+ name,
                        method: 'post',
                        dataType : 'json',
                        success: function(resp) {
                            if (resp.success == false) {
                                $btn.parents('div.result-wrapper').find('.result-output').first().html();
                                alert(resp.message);
                            } else {
                                $btn.parents('div.result-wrapper').find('.result-output').first().html(resp.message);
                            }
                            $btn.button('reset');
                            $('button, input').attr('disabled', false);
                            $('#quickyloader').hide();
                        }
                    });
                }
            });

            $('button.alt-c').click(function(){
                var $btn = $(this);
                var pid = $btn.parents('div.result-wrapper').find('input.result-input').first().val();
                if  (pid=='') {
                    alert('Please Enter The Required Input Field');
                } else {
                    $('#quickyloader').show();
                    $('button, input').attr('disabled', true);
                    $btn.button('loading');
                    $.ajax({
                        url:'./handler.php?key='+$(this).data('key'),
                        data:'pid='+ pid,
                        method: 'post',
                        dataType : 'json',
                        success: function(resp) {
                            if (resp.success == false) {
                                $('#loadprod').html('');
                                alert(resp.message);
                            } else {
                                $('#loadprod').html(resp.message);
                            }
                            $btn.button('reset');
                            $('#quickyloader').hide();
                            $('button, input').attr('disabled', false);
                        }
                    });
                }
            });
		});
	</script>
</body>
</html>